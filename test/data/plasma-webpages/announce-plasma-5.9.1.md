---
title: "KDE Plasma 5.9.1, Bugfix Release for January"
description: Today KDE releases a Bugfix update to KDE Plasma 5
date: 2017-02-14
changelog: plasma-5.9.0-5.9.1-changelog
layout: plasma
draft: true
---

{{< plasma-5-9-video >}}

Tuesday, 14 February 2017.

{{< i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.9.1" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in January with many feature refinements and new modules to complete the desktop experience." "5.9" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

FIXME
